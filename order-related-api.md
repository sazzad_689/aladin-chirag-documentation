# customer signup


> If a cart is edited by admin or store
```
                        axios({
                            method:'post',
                            url:'https://us-central1-aladins-chirag.cloudfunctions.net/sendCartChangedEmail',
                            data: {
                                order : self.order,
                                type : 'cart-edited-by-store',      <- cart-edited-by-store || cart-edited
                                store : ''                          <- store name if cart is edited by store
                            }
                        })

```

> to charge a card
```
                    axios({
                            method:'post',
                            url:'https://us-central1-aladins-chirag.cloudfunctions.net/chargeStripe',
                            data: {
                                customerId: customerID,
                                amount: totalAmount
                            }
                        })

```

> Send payment receipt to customer
```
                        axios({
                                method:'post',
                                url:'https://us-central1-aladins-chirag.cloudfunctions.net/sendPaymentReceipt',
                                data: {
                                    order : self.order,
                                    charge : totalAmount,
                                    payment_option : 'Pay Now By Card'
                                }
                            })
```

> Payment confirmation
```
                                axios({
                                        method:'post',
                                        url:'https://us-central1-aladins-chirag.cloudfunctions.net/paymentConfirmation',
                                        data: {
                                            MailTo : email,
                                            name : name,
                                            orderID: self.order.ref,
                                            charge: totalAmount
                                        }
                                    })
```

> Update stock count
```
            axios({
                    method: 'post',
                    url: 'https://us-central1-aladins-chirag.cloudfunctions.net/updateStockCount',
                    data: {
                        productID: updateQuantity.productID,
                        decrease : Number(updateQuantity.quantity)
                    }
                })
```

> sent final invoice
```
    axios({
                                        method: 'post',
                                        url: 'https://us-central1-aladins-chirag.cloudfunctions.net/sendFinalInvoice',
                                        data: {
                                            order: self.order,
                                        }
                                    })
```

> Generate final invoice
```
axios({
                                    method: 'post',
                                    url: 'https://us-central1-aladins-chirag.cloudfunctions.net/generateFinalInvoice',
                                    data: {
                                        order: self.order,
                                    }
                                })
```

