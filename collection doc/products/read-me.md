
> ``categoryID`` id from collection ``store-product-categry``

> ``related_products`` an Array , lists the products id of that store that are similar in nature selected by admin

> ``variants``
* ``optionType`` describe the variant type. value is either ``checkbox``  or ``radio``
* ``type`` describe this variant selection type. value is either ``mandatory`` or ``optional``


> the code used to generate ``indexArr``
```
let size = self.product.name.length;
                let indexArr = [];
                for(var i=1 ; i<= size ; i++){
                    indexArr.push(self.product.name.substr(0, i).toLowerCase())
                }

                self.product.name.split(' ').forEach(item => {
                    if(!indexArr.includes(item))
                        indexArr.push(item.toLowerCase())
                });

                self.product.name.split(' ').forEach(item => {
                    var temp_size = item.length;
                    for(var i=1 ; i<= temp_size ; i++){
                        var check = item.substr(0, i).toLowerCase();
                        if(!indexArr.includes(check))
                            indexArr.push(check)
                    }
                });

                self.product.indexArr = indexArr;
```
       