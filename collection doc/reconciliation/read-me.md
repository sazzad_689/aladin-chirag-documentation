> ``PaymentRecordID`` the id of the document in ``payment_record`` collection

> ``customerID`` the id/uid of customer/user from ``users`` document

> ``deliveryOptionId`` selected delivery option id from ``delivery-methods``

> ``paymentOptionId`` selected payment option id from ``system-payment-options``

> ``storeID`` the id of store from ``stores`` collection

> ``reconcileRef`` this refers to the document id of ``reconciliation-reference`` document. each ``reconciliation`` document is connected to one of the document of ``reconciliation-reference`` id

