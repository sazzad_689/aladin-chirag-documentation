
> ``delivery_methods`` lists of selected delivery methods for this shop. refers to id of document from ``delivery-methods`` collection

> ``docPATH`` contract document file path in firebase storage
> ``docURL`` contract document downloadable url

> ``membership`` available option are ``Basic`` , ``Gold`` , ``Premium``

> ``payment_methods`` lists of selected payment methods for this shop. refers to id of document from ``system-payment-option`` collection

> ``postcodes`` list of postcodes where this shop ``Does not give service``

> ``serviceCatID`` refers to the id of document from ``service-category`` collection

> ``serviceID`` refers to the id of document from ``services`` collection


> ``userID`` refers to the id/uid of user from ``users`` collection who is the owner of this store.

