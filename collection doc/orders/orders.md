# important points about order document


> initially when we first enter this document into firestore then the value of `id` is empty string '' . after the insertion we get the doc id returned by firestore. we then take that id and update the doc `id` key with that given id. This will trigger an event on firebase cloud fucntion which will cause all the necessary thing for a new order i.e set the `ref` key, generate temporaray invoice, send email, generate push notification etc.

> we send `ref` as empty.

> `customer` customer id for this order

> `deliveryDateInt` unix time * 1000

> `orderDate` unix time * 1000

> `orderDateText` text representation of orderDate similar to front end. thsi one is need to show order date text on invoices

> `status` initial statuse are always  `order-received`

> `storeIDS` an Array. collection of store ids from which products were taken for this order.

> `productIDS` an Array. all products ids of this order.

> `orderStatus` a map . the `key` is store id , and the value is intially `order-received` . for multistore order it will look like 

    
    "orderStatus":{
          "hv6qYdW2a7dszHUbEbsC" : "order-received",
          "another-store-id" : "order-received",
          "another-store-id" : "order-received"
       }
       
> `shopZoneId` id of document from collection `service-zones`


> `order.cart` the main cart object . an Array , each item representing an product for this order 
    
    {
                "adjustedAmount":0,
                "buyLimit":0,
                "categoryID":"L0ULukxuNZaoP6UGzI3y",
                "discount":"",
                "discountType":"no",
                "discountedPrice":20,
                "grandTotalPrice":"20.00",
                "gst":10,
                "ignorestock":true,
                "images":[
                   {
                      "imagePATH":"product-images/7BqoEnnnlHM0eVg6sXgl/1539081153217chicken.jpeg",
                      "imageURL":"https://firebasestorage.googleapis.com/v0/b/aladins-chirag.appspot.com/o/product-images%2F7BqoEnnnlHM0eVg6sXgl%2F1539081153217chicken.jpeg?alt=media&token=5e6e8cbd-8523-4feb-853d-570f0db10fdf"
                   }
                ],
                "instruction":"",
                "minQuantity":1,
                "multistoresupport":true,
                "options":[
    
                ],
                "price":20,
                "productID":"7BqoEnnnlHM0eVg6sXgl",
                "productName":"Chicken Breast",
                "productQty":1,
                "productType":"product",
                "serviceID":"RMeg1TETv3dTTuOcIGuO",
                "stock":0,
                "storeID":"hv6qYdW2a7dszHUbEbsC",
                "storeName":"Agora",
                "subcategoryID":"hZC47d6T8c1VkWgPWqmR",
                "substitute":{
    
                },
                "substituteMode":false,
                "totalPrice":20,
                "unit":"packet",
                "weight":0
             }
    
   
   
   > on this object two things may change , the `options` key if this item has variant, and `substitute` key if a substitute of this product is selected by customer.
   
   > the `substitute` key will have the exact replica of one item of cart array
   
   > the substiture key value will be like 
    
    [  
       {  
          "variantID":"RHIRKXSUSJHQ",
          "optName":"chittagiong",
          "optQty":1,
          "price":2,
          "totalPrice":2,
          "variantRequird":"mandatory"
       },
       {  
          "variantID":"HTGZNCUARWEL",
          "optName":"Medium",
          "optQty":1,
          "price":0,
          "totalPrice":0,
          "variantRequird":"mandatory"
       }
    ]
    
    
 
  > `deliveryAddress` is selected item from the users `deliveryAddress` key array . see a document from users collection where user type is customer
  
  > `order.shopZoneInfo` is not necessary i think. it will be removed
    



