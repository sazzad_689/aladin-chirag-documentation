## important notes about ``fcm-notifications`` collection

> ``relevantArray`` lists of users who will get this notification

> ``subjectId`` eithe id of ``orders`` collection or id of ``store`` collection or id of ``reconciliation-reference``

> ``subjectType`` available options are ``order``, ``store`` , ``reconciliation``