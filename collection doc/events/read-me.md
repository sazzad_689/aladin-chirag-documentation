## important notes about ``events`` collection


> ``Notallowed`` and ``allowed`` keys are array which hold list of dos and dons and each of them has this format
```
    {
      "description": "Smoking is strictly prohibited"
    }
```

> ``imagePATH`` is the uploaded image file path on firebase file storage. prefered path is ``event/event_id/actual_file``

> ``imageURL`` uploaded file url generated using firebase to show the file/image publicly