> ``categoryID`` id of document belongs to collection ``store-product-category``

> ``orderID`` id of document belongs to collection ``orders``

> ``productID`` id of document belongs to collection ``products``

> ``storeID`` id of document belongs to collection ``store``

> ``subcategoryID`` id of document belongs to collection ``store-product-category``

