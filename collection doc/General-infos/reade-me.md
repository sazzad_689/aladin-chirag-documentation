##Notes abot ```General-infos``` collection

> we have 10 types of document in this collection. Each of them is classified by type. avaibale types are
``general-info`` , ``country-location`` , ``faq`` ,``delivery-information`` , ``get-product-info``,
``how-it-works`` , ``privacy-policy`` , ``refund-policy`` , ``site-about-us`` , ``terms-conditions``

> type ``general-info`` holds all general info abotu this site i.e site logo, homepage banner, primary email, phone number etc

> type ``country-location`` holds the country name for this system . each of this type has a sub collection which holds the postcodes for that respective country

> type ``get-product-info`` holds the section info which exists just under the main banner in homepage

> type ``faq`` holds the faqs section of this site.

> others type holds a paragraph to show text on their respective pages