> ``customerID`` the id/uid of customer/user from ``users`` document

> ``deliveryOptionId`` selected delivery option id from ``delivery-methods``

> ``paymentOptionId`` selected payment option id from ``system-payment-options``

> ``storeID`` the id of store from ``stores`` collection

