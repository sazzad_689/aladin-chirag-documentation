## important notes about ``fcm-tokens`` collection

> ``userID`` doc id of the ``users`` collection which belongs to that user.

> ``userType`` available options are ``admin`` , ``store`` , ``customer``

> ``tokenFor`` available options are ``web`` , ``mobile``