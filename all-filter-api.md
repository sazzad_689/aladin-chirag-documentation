# All filters api


> Customer filter
```
                        axios({
                                                method:'post',
                                                url:'https://us-central1-aladins-chirag.cloudfunctions.net/filterCustomers',
                                                data: {
                                                    offset: 0,
                                                    dataPerPage : 50,
                                                    email : self.searchFilter.email,
                                                    mobile : self.searchFilter.mobile,
                                                }
                                            })

```

> Filter notification
```
axios({
                        method:'post',
                        url:'https://us-central1-aladins-chirag.cloudfunctions.net/filterNotification',
                        data: {
                            offset: 0,
                            dataPerPage : 6000,
                            fromDate : self.startAt !== '' ? (new Date(self.startAt).getUnixTime() * 1000 ) : '',
                            toDate : self.endAt !== '' ? (new Date(self.endAt).getUnixTime() * 1000 ) : '',
                        }
                    })
```

> Filter voucher
```
axios({
                        method:'post',
                        url:'https://us-central1-aladins-chirag.cloudfunctions.net/allVouchers',
                        data: {
                            offset: 0,
                            dataPerPage : self.dataPerPage,
                            fromDate : self.searchFrom !== '' ? (new Date(self.searchFrom).getUnixTime() * 1000 ) : '',
                            toDate : self.searchTo !== '' ? (new Date(self.searchTo).getUnixTime() * 1000 ) : '',
                            filterBy : self.filterBy,
                            searchFor : self.searchKey.toLowerCase()
                        }
                    })
```

> Filter store
``` 
var last_30_days = (new Date('australian_time')).getTime() - self.getMiliseconds(30);
 axios({
                         method:'post',
                         url:'https://us-central1-aladins-chirag.cloudfunctions.net/stores',
                         data: {
                             offset: 0,
                             dataPerPage : self.dataPerPage,
                             searchFor : self.searchKey.toLowerCase()
                         }
                     })
 ```
 
 > Filter store review
 ```
 axios({
                         method:'post',
                         url:'https://us-central1-aladins-chirag.cloudfunctions.net/filterReviews',
                         data: {
                             offset: 0,
                             storeID : self.store.id,
                             range : last_30_days,
                             dataPerPage : self.dataPerPage,
                         }
                     })
 ```
 
 > Filter search history
 
 ```
 axios({
                         method:'post',
                         url:'https://us-central1-aladins-chirag.cloudfunctions.net/filterSearchHistory',
                         data: {
                             offset: 0,
                             dataPerPage : 20000,
                             key : self.searchFilter.key.trim(),
                             storeID : self.searchFilter.storeID,
                             serviceID: self.searchFilter.serviceID
                         }
                     })
 ```
 
 > Filter Order
 ```
 axios({
                         method:'post',
                         url:'https://us-central1-aladins-chirag.cloudfunctions.net/filterOrders',
                         data: {
                             offset: 0,
                             dataPerPage : 2000,
                             fromDate : self.searchFilter.startAt !== '' ? (new Date(self.searchFilter.startAt).getUnixTime() * 1000 ) : '',
                             toDate : self.searchFilter.endAt !== '' ? (new Date(self.searchFilter.endAt).getUnixTime() * 1000 ) : '',
                             orderStatus : self.searchFilter.orderStatus,
                             storeID : self.searchFilter.storeID,
                             orderID: self.searchFilter.orderID,
                             shopZoneId : self.searchFilter.shopZoneId,
                             deliveryDate : self.searchFilter.deliveryDate !== '' ? (new Date(self.searchFilter.deliveryDate).getUnixTime() * 1000 ) : '',
                             ref : self.searchFilter.ref
                         }
                     })
 ```
 
 > order pagination
 ```
 axios({
                     method:'post',
                     url:'https://us-central1-aladins-chirag.cloudfunctions.net/allOrders',
                     data: {
                         offset: startAfter,
                         dataPerPage : self.dataPerPage
                     }
                 })
 ```
 
 > Filter payments
 ```
 axios({
                         method:'post',
                         url:'https://us-central1-aladins-chirag.cloudfunctions.net/filterPayments',
                         data: {
                             offset: 0,
                             dataPerPage : self.dataPerPage,
                             paymentOption : self.paymentOption,
                             deliveryOption : self.deliveryOption,
                             fromDate : self.searchFilter.startAt !== '' ? (new Date(self.searchFilter.startAt).getUnixTime() * 1000 ) : '',
                             toDate : self.searchFilter.endAt !== '' ? (new Date(self.searchFilter.endAt).getUnixTime() * 1000 ) : '',
                             storeID : self.searchFilter.storeID,
                             ref: self.searchFilter.orderID
                         }
                     })
 ```
 
 > Payments pagination
 ```
 axios({
                     method:'post',
                     url:'https://us-central1-aladins-chirag.cloudfunctions.net/allPayments',
                     data: {
                         offset: startAfter,
                         dataPerPage : self.dataPerPage
                     }
                 })
 ```
 
 > Filter reconciled record
 ```
 axios({
                         method:'post',
                         url: `https://us-central1-aladins-chirag.cloudfunctions.net/filterNewReconcile`,
                         data: {
                             offset: 0,
                             dataPerPage : 200,
                             fromDate : self.searchFilter.startAt !== '' ? (new Date(self.searchFilter.startAt).getUnixTime() * 1000 ) : '',
                             toDate : self.searchFilter.endAt !== '' ? (new Date(self.searchFilter.endAt).getUnixTime() * 1000 ) : '',
                             storeID : self.searchFilter.storeID,
                             ref: self.searchFilter.orderID
                         }
                     })
 ```
 > Paginate reconciled record
 ```
axios({
                    method:'post',
                    url:'https://us-central1-aladins-chirag.cloudfunctions.net/ReconciledReferences',
                    data: {
                        offset: startAfter,
                        dataPerPage : self.dataPerPage
                    }
                })

```
 
 > un reconciled payments pagination
 
 ```
 axios({
                     method:'post',
                     url:'https://us-central1-aladins-chirag.cloudfunctions.net/ReconciledPayments',
                     data: {
                         offset: startAfter,
                         dataPerPage : self.dataPerPage
                     }
                 })
 ```
 
 
 > Filter sales
 ```
 axios({
                         method:'post',
                         url:'https://us-central1-aladins-chirag.cloudfunctions.net/filterSales',
                         data: {
                             offset: 0,
                             dataPerPage : 5000,
                             storeID : self.searchFilter.storeID,
                             ref : self.searchFilter.orderID,
                             productID : self.searchFilter.productID,
                             fromDate : self.searchFilter.startAt !== '' ? (new Date(self.searchFilter.startAt).getUnixTime() * 1000 ) : '',
                             toDate : self.searchFilter.endAt !== '' ? (new Date(self.searchFilter.endAt).getUnixTime() * 1000 ) : '',
                         }
                     })
 ```
 
 > sales pagination
 ```
 axios({
                     method:'post',
                     url:'https://us-central1-aladins-chirag.cloudfunctions.net/allSales',
                     data: {
                         offset: startAfter,
                         dataPerPage : self.dataPerPage
                     }
                 })
 ```
 
 > Filter store sales and pagination
 
 ```
 axios({
                         method:'post',
                         url:'https://us-central1-aladins-chirag.cloudfunctions.net/filterStoreSales',
                         data: {
                             offset: 0,
                             dataPerPage : 5000,
                             storeID : self.storeID,
                             ref : self.searchFilter.orderID,
                             categoryID : self.searchFilter.categoryID,
                             subcategoryID : self.searchFilter.subcategoryID,
                             productID : self.searchFilter.productID,
                             fromDate : self.searchFilter.startAt !== '' ? (new Date(self.searchFilter.startAt).getUnixTime() * 1000 ) : '',
                             toDate : self.searchFilter.endAt !== '' ? (new Date(self.searchFilter.endAt).getUnixTime() * 1000 ) : '',
                         }
                     })
 
 ```
 
 > products filter for a store
 ```
 axios({
                         method:'post',
                         url:'https://us-central1-aladins-chirag.cloudfunctions.net/filterProducts',
                         data: {
                             storeID: this.storeID,
                             offset: 0,
                             dataPerPage : 10000,
                             searchIN : self.searchIN,
                             searchORDER : self.searchORDER,
                             searchFor : self.searchKey.toLowerCase()
                         }
                     })
 ```
 
 > Products list pagination for a store
 
 ```
 axios({
                     method:'post',
                     url:'https://us-central1-aladins-chirag.cloudfunctions.net/storeProducts',
                     data: {
                         storeID: this.storeID,
                         offset: startAfter,
                         dataPerPage : self.dataPerPage
                     }
                 })
 ```


