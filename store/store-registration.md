# customer signup

> the new user map

``` bash
let new_store_user = {
             name: '',
             email: '',
             phone: '',
             uid: data.user.uid,   <- the user id returned by firebase after successfull signup
             verified: false,
             verification_code: verification_code,    <- random big string chr + num
             verifiedBy : 'Email',   <-- options = ['Email' , 'Phone' , 'Facebook' , 'Google']
             marchantID : '',
             imageURL: '',
             imagePATH: '',
             status: 'pending',    <-- options = ['pending' , 'active' , 'banned' ]
             type: 'shop',     <-- options = ['customer' , 'admin' , 'shop' ]
             storeID : '',
             deliveryAddress:[],
             address: '',
             mobileNotification: true,
             emailNotification: true,
          };
```

> after user data in the ``users`` collection we send verification email to store email
```
axios({
           method: 'post',
           url: 'https://us-central1-aladins-chirag.cloudfunctions.net/sendVerificationEmail',
           data: {
             Name : '',
             Link : root, <- root_url+'/userverify/'+ verification_code
             MailTo : email
           }
         })

```
> we then generate a document for the store and save that to ``store`` collection. Please see ``store.json`` to check the store document structure.


> we then create a ``marchantID`` for that sotre and save that to that store . ``marchantID`` format is like this ``random 4 chars + (current_store_count + 1) + random 2 chars``


> when user click on verification email link then if it gets verified correctly then we send him confirmation email. this is code for sending confirmation email.
``
  
              if(data.type === 'shop'){
                  mailText = 'your shop account has been successfully verified. You will get a marchant ID as soon as your shop got approval from our authorities.';
              } else {
                  mailText = 'your account has been successfully verified.';
              }
  
              axios({
                  method:'post',
                  url:'https://us-central1-aladins-chirag.cloudfunctions.net/sendVerificationConfirmationEmail',
                  data: {
                      name : data.name,
                      Link : dashboard_link,
                      MailTo : data.email,
                      type : data.type,
                      mailText : mailText
                  }
              })``

> some of the keys of store document which needs some explanation are give below
 - ``delivery_methods : []`` the delivery methods id from ``delivery_methods`` collection which is selected for this store
 - ``payment_methods : []`` the payment methods id from ``system-payment-options`` collection which is selected for this store
 - ``postcodes : []`` the postcodes where this shop doesn't provide service
 - ``indexArr : []`` here we are splitting the store name to get better search result