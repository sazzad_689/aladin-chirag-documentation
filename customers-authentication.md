# customer signup

> the new user map

``` bash
let new_user = {
             name: '',
             email: '',
             phone: self.countryCode + self.customer.phone,
             uid: data.user.uid,   <- the user id returned by firebase after successfull signup
             verified: false,
             verification_code: verification_code,    <- random big string chr + num
             verifiedBy : 'Email',   <-- options = ['Email' , 'Phone' , 'Facebook' , 'Google']
             marchantID : '',
             imageURL: '',
             imagePATH: '',
             status: 'pending',    <-- options = ['pending' , 'active' , 'banned' ]
             type: 'customer',     <-- options = ['customer' , 'admin' , 'shop' ]
             storeID : '',
             deliveryAddress:[],
             address: '',
             free_delivery: 0,
             points: 0,
             mobileNotification: true,
             emailNotification: true,
          };
```

> after user data in the ``users`` collection we send verification email to user
```
axios({
           method:'post',
           url:'https://us-central1-aladins-chirag.cloudfunctions.net/sendVerificationEmail',
           data: {
             Name : '',
             Link : root, <- root_url+'/userverify/'+ verification_code
             MailTo : email
           }
         })

```
> we then generate a `` ch + num length 7`` referral code for this customer and save it to ``voucher-promo-referal`` collection. for referral code the document structure is this
```
{
             action: '',
             code: '',,
             createdAt : unix_date_number,
             enabled : true,
             id : 'document_id',
             type : 'referrel',
             uid : the_customer_id,
             user_type : 'customer',
             validFor : 'customer'
          };

```
